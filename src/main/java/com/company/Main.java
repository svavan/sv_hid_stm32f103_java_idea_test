package com.company;

import org.hid4java.*;

import javax.usb.*;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Main {

    static int Crc8Table[] = {0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97,
            0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E,
            0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4,
            0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
            0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11,
            0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
            0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52,
            0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
            0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA,
            0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
            0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9,
            0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
            0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C,
            0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
            0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F,
            0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
            0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED,
            0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
            0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE,
            0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
            0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B,
            0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
            0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28,
            0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
            0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0,
            0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
            0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93,
            0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A,
            0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56,
            0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
            0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15,
            0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC};

    //===============================================================================================

    public static void main(final String[] args) throws UsbException, InterruptedException, UnsupportedEncodingException {

        testWitchHid4Java();

        //testWitchUsb4Java();

    }

    //===============================================================================================

    public static void testWitchHid4Java() throws UsbException, InterruptedException, UnsupportedEncodingException {

        // Configure to use custom specification
        HidServicesSpecification hidServicesSpecification = new HidServicesSpecification();
        hidServicesSpecification.setAutoShutdown(true);
        hidServicesSpecification.setScanInterval(32);
        hidServicesSpecification.setPauseInterval(0);
        hidServicesSpecification.setScanMode(ScanMode.SCAN_AT_FIXED_INTERVAL_WITH_PAUSE_AFTER_WRITE);
        //hidServicesSpecification.setScanMode(ScanMode.SCAN_AT_FIXED_INTERVAL);
        //hidServicesSpecification.setScanMode(ScanMode.NO_SCAN);

        // Get HID services using custom specification
        HidServices hidServices = HidManager.getHidServices(hidServicesSpecification);
        //hidServices.addHidServicesListener(this);

// Provide a list of attached devices
        for (HidDevice hidDevice : hidServices.getAttachedHidDevices()) {
            System.out.println(hidDevice);
        }

// Open a Bitcoin Trezor device by Vendor ID and Product ID with wildcard serial number
        HidDevice crutch = hidServices.getHidDevice(0x0483, 0x1260, null);

        int i = 0;
        boolean weightIsRequested = false;
        byte annunciatorIndex = 0;

        byte[] bufTx;
        byte[] bufRx = new byte[63];
        int sent;

        while (true) {
            try {
                if (!weightIsRequested)
                {
                    weightIsRequested = true;
                    bufTx = creatPackForSendHid(new byte[]{0x05, 0x11}, 1);
                    sent = crutch.write(bufTx, bufTx.length, (byte) 1);
                    System.out.println(sent + " bytes sent");
                    System.out.println(prnBytesArrayToString(bufTx, bufTx.length, false) + "\r\n");
                }

                int received = crutch.read(bufRx);

                //System.out.print("\n" + received + " bytes received, \n");
                //System.out.println(prnBytesArrayToString(bufRx, received, false) + "\r\n");

                if (Crc8Check(bufRx, received))
                {
                    System.out.print("\n" + received + " bytes received, \n");
                    System.out.println(prnBytesArrayToString(bufRx, received, false) + "\r\n");

                    if (bufRx[0] == 0x04) { // Ответ от весов
                        weightIsRequested = false;
                        byte[] weight = getWeightSubString(bufRx);
                        bufTx = creatPackForSendHid(new byte[]{0x1B, 0x48, 0x14}, 3);
                        sent = crutch.write(bufTx, bufTx.length, (byte) 3);
                        System.out.println(sent + " bytes sent");
                        System.out.println(prnBytesArrayToString(bufTx, bufTx.length, false) + "\r\n");
                        bufTx = creatPackForSendHid(weight, 3);
                        sent = crutch.write(bufTx, bufTx.length, (byte) 3);
                        System.out.println(sent + " bytes sent");
                        System.out.println(prnBytesArrayToString(bufTx, bufTx.length, false) + "\r\n");
                    }

                    if (bufRx[0] == 0x05) { // Ответ от сканера

                        byte[] barCode = getBarCodeSubString(bufRx);
                        bufTx = creatPackForSendHid(new byte[]{0x1B, 0x48, 0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x1B, 0x48, 0x00}, 3);
                        sent = crutch.write(bufTx, bufTx.length, (byte) 3);
                        //System.out.println(sent + " bytes sent");
                        bufTx = creatPackForSendHid(barCode, 3);
                        sent = crutch.write(bufTx, bufTx.length, (byte) 3);
                        //System.out.println(sent + " bytes sent");
                }

                    if (bufRx[0] == 0x06) { // Ответ от панели, сто было бы странно.
                    }
                } else {
                    System.out.print(received + " ");
                    if (i++ == 20) {
                        i = 0;
                        System.out.print("\r");

                        if (annunciatorIndex < 20) {
                            bufTx = creatPackForSendHid(new byte[]{0x1F, 0x23, 0x01, annunciatorIndex}, 3);
                            sent = crutch.write(bufTx, bufTx.length, (byte) 3);
                            //System.out.println(prnBytesArrayToString(bufTx, bufTx.length, false) + "\r\n");
                            //System.out.println(sent + " bytes sent");
                            annunciatorIndex++;
                        }

                        if (annunciatorIndex >= 20) {
                            bufTx = creatPackForSendHid(new byte[]{0x1F, 0x23, 0x00, (byte) (20 - (annunciatorIndex - 20))}, 3);
                            sent = crutch.write(bufTx, bufTx.length, (byte) 3);
                            //System.out.println(prnBytesArrayToString(bufTx, bufTx.length, false) + "\r\n");
                            //System.out.println(sent + " bytes sent");
                            annunciatorIndex++;
                            if (annunciatorIndex > 40) annunciatorIndex = 0;
                        }

                    }
                }
            } catch (Exception e) {
                hidServices.shutdown();
            }
        }


// Clean shutdown
        //hidServices.shutdown();
    }

    //===============================================================================================

    public static void testWitchUsb4Java() throws UsbException, InterruptedException, UnsupportedEncodingException {

        // Get the USB services and dump information about them

        final UsbServices services = UsbHostManager.getUsbServices();
        UsbHub hub = services.getRootUsbHub();
        System.out.println(hub);
        UsbDevice device;
        device = findDevice(hub, (short) 0x0483, (short) 0x1260);
        UsbConfiguration configuration = device.getActiveUsbConfiguration();
        System.out.println(configuration);
        UsbInterface iface = configuration.getUsbInterface((byte) 0);

        System.out.println(iface);

        //barCodeScaner(iface);
        //getWeight(iface);
        //dpd201Time(iface);
        //writeInDPD201(iface, new byte[] {0x41});
        //usbWrite(iface,new byte[] {0x0d},3 );
        //parseWeight(new byte[] {0x04,0x06,0x01,0x02,0x41,0x20,0x20,0x30,0x2E,0x31,0x33,0x36,0x03,0x04,0x15,0x15,0x15,0x15});

        testAll1hUsb4Java(iface);
    }

    //===============================================================================================

    public static byte[] creatPackForSend(byte[] dataPacket, int uartNum) {

        byte bufTx[] = new byte[63];

        int lenght = 0;

        if (dataPacket.length < bufTx.length) lenght = dataPacket.length;
        else lenght = bufTx.length - 1;

        if (uartNum == 3) {
            for (int j = 0; j < lenght; j++)
                bufTx[j + 2] = dataPacket[j];

            bufTx[1] = (byte) lenght;
        } else
            for (int j = 0; j < lenght; j++) bufTx[j + 1] = dataPacket[j];

        uartNum = uartNum & 0x00000003;
        bufTx[0] = (byte) uartNum;

        bufTx[bufTx.length - 1] = (byte) Crc8(bufTx, bufTx.length - 1);

        return bufTx;
    }

    //===============================================================================================

    public static byte[] creatPackForSendHid(byte[] dataPacket, int uartNum) {

        int dataPacketSize = 62;

        byte bufTx[] = new byte[dataPacketSize];

        int lenght = 0;

        if (dataPacket.length < bufTx.length)
            lenght = dataPacket.length;
        else
            lenght = bufTx.length - 1;

        if (uartNum == 3)
        {
            for (int j = 0; j < lenght; j++)
                bufTx[j + 1] = dataPacket[j];

            bufTx[0] = (byte) lenght;
        }
        else {
            for (int j = 0; j < lenght; j++)
                bufTx[j + 0] = dataPacket[j];
        }

       //uartNum = uartNum & 0x00000003;
        //bufTx[0] = (byte) dataPacketSize;

        bufTx[bufTx.length - 1] = (byte) Crc8(bufTx, bufTx.length - 1);

        return bufTx;
    }

    //===============================================================================================
    public static void testAll1hUsb4Java(UsbInterface iface) throws UsbException {

        int i = 0;
        boolean weightIsRequested = false;
        byte annunciatorIndex = 0;

        iface.claim();

        UsbEndpoint readEndpoint = iface.getUsbEndpoint((byte) 0x81);
        UsbPipe readPipe = readEndpoint.getUsbPipe();

        UsbEndpoint writeEndpoint = iface.getUsbEndpoint((byte) 0x01);
        UsbPipe writePipe = writeEndpoint.getUsbPipe();

        readPipe.open();
        writePipe.open();

        byte[] bufTx;
        byte[] bufRx = new byte[63];

        while (true) {
            try {
                if (!weightIsRequested) {
                    weightIsRequested = true;
                    bufTx = creatPackForSend(new byte[]{0x05, 0x11}, 1);
                    int sent = writePipe.syncSubmit(bufTx);
                    System.out.println(sent + " bytes sent");
                }

                int received = readPipe.syncSubmit(bufRx);

                System.out.print("\n" + received + " bytes received, \n");
                System.out.println(prnBytesArrayToString(bufRx, received, false) + "\r\n");

                if (Crc8Check(bufRx, received)) {
                    System.out.print("\n" + received + " bytes received, \n");
                    System.out.println(prnBytesArrayToString(bufRx, received, false) + "\r\n");

                    if (bufRx[0] == 0x04) { // Ответ от весов
                        weightIsRequested = false;
                        byte[] weight = getWeightSubString(bufRx);
                        bufTx = creatPackForSend(new byte[]{0x1B, 0x48, 0x14}, 3);
                        int sent = writePipe.syncSubmit(bufTx);
                        bufTx = creatPackForSend(weight, 3);
                        sent = writePipe.syncSubmit(bufTx);
                    }

                    if (bufRx[0] == 0x05) { // Ответ от сканера

                        byte[] barCode = getBarCodeSubString(bufRx);
                        bufTx = creatPackForSend(new byte[]{0x1B, 0x48, 0x00, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x1B, 0x48, 0x00}, 3);
                        int sent = writePipe.syncSubmit(bufTx);
                        bufTx = creatPackForSend(barCode, 3);
                        sent = writePipe.syncSubmit(bufTx);
                    }

                    if (bufRx[0] == 0x06) { // Ответ от панели, сто было бы странно.
                    }
                } else {
                    System.out.print(received + " ");
                    if (i++ == 5) {
                        i = 0;
                        System.out.print("\r");


                        if (annunciatorIndex < 20) {
                            bufTx = creatPackForSend(new byte[]{0x1F, 0x23, 0x01, annunciatorIndex}, 3);
                            int sent = writePipe.syncSubmit(bufTx);
                            annunciatorIndex++;
                        }

                        if (annunciatorIndex >= 20) {
                            bufTx = creatPackForSend(new byte[]{0x1F, 0x23, 0x00, (byte) (20 - (annunciatorIndex - 20))}, 3);
                            int sent = writePipe.syncSubmit(bufTx);
                            annunciatorIndex++;
                            if (annunciatorIndex > 40) annunciatorIndex = 0;
                        }
                    }
                }
            } catch (Exception e) {
                readPipe.close();
                writePipe.close();
                iface.release();
            }
        }
    }
    //===============================================================================================
    public static double parseWeight (byte[] rxBuf){
        double weight = 0;
        //String weightString = new String(rxBuf);
        rxBuf = getWeightSubString(rxBuf);
        weight = Double.parseDouble(new String(rxBuf));
        //weight.getBytes("Cp866")
        return weight;
    }
    //===============================================================================================

    public static byte[] getWeightSubString (byte[] rxBuf){

        byte res[] = new byte[ ]{0x20,0x20,0x30,0x2E,0x30,0x30,0x30};

        int i;

        for(i=0;i<(rxBuf.length-7);i++){
            if((rxBuf[i]==0x01)&&(rxBuf[i+1]==0x02))break;
        }

        i=i+3;

        for(int j=0;j<7;j++)
            res[j]=rxBuf[j+i];

        return res;
    }

    //===============================================================================================

    public static byte[] getBarCodeSubString (byte[] rxBuf){

        //byte res[] = new byte[ ]{0x20,0x20,0x30,0x2E,0x30,0x30,0x30};

        int startIndex;
        int endIndex;

        for(startIndex=0;startIndex<rxBuf.length;startIndex++){
            if((rxBuf[startIndex]>0x19)&&(rxBuf[startIndex]<0x5B))break;
        }

        for(endIndex=startIndex+1;endIndex<rxBuf.length;endIndex++){
            if((rxBuf[endIndex]<0x20)||(rxBuf[endIndex]>0x5A))break;
        }

        int len = endIndex - startIndex + 1;

        byte [] res = new byte[len];

        for(int j=0;j<len;j++)
            res[j]=rxBuf[startIndex+j];

        return res;
    }


    //===============================================================================================

    public static void barCodeScaner(UsbInterface iface) throws UsbException {
        usbRead(iface,(byte)0x81,false);
    }
    //===============================================================================================

    public static void getWeight(UsbInterface iface) throws UsbException {
        usbWrite(iface,new byte[] {0x05,0x11},1 );
        usbRead(iface,(byte)0x81,true);
    }
    //===============================================================================================

    public static void dpd201Time(UsbInterface iface) throws UsbException, InterruptedException, UnsupportedEncodingException {

        //writeInDPD201(iface, new byte[]{0xC});

        while(true)
        {

            String timeStamp = getCurrentTimeStamp();

            writeInDPD201(iface, new byte[]{0x1B,0x48,0x00});

            writeInDPD201(iface, timeStamp.getBytes("Cp866"));
            //Thread.sleep(1000);
        }
    }
    //===============================================================================================
    public static String getCurrentTimeStamp() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
    }

    //===============================================================================================

    public static void writeInDPD201(UsbInterface iface, byte[] buff) throws UsbException {
        usbWrite(iface,buff,3 );
    }
    //===============================================================================================
    public static UsbDevice findDevice(UsbHub hub, short vendorId, short productId)
    {
        for (UsbDevice device : (List<UsbDevice>) hub.getAttachedUsbDevices())
        {
            UsbDeviceDescriptor desc = device.getUsbDeviceDescriptor();
            if (desc.idVendor() == vendorId && desc.idProduct() == productId) return device;
            if (device.isUsbHub())
            {
                device = findDevice((UsbHub) device, vendorId, productId);
                if (device != null) return device;
            }
        }
        return null;
    }

//===============================================================================================
    public static void usbWrite(UsbInterface iface) throws UsbException {


            iface.claim();
            try {
                UsbEndpoint endpoint = iface.getUsbEndpoint((byte) 0x01);
                UsbPipe pipe = endpoint.getUsbPipe();
                System.out.println(pipe);
                pipe.open();
                try {
                       byte bufTx[] = new byte[63];
                        for(byte i=1;i<63;i++)bufTx[i]=i;
                       bufTx[0]=3;
                        //bufTx[1]=100;

                        //int sent = pipe.syncSubmit(new byte[]{0x01, 0x01});
                        int sent = pipe.syncSubmit(bufTx);
                        System.out.println(sent + " bytes sent");
                } finally {
                    pipe.close();
                }
            } finally {
                iface.release();
            }

    }

    //===============================================================================================
    public static void usbWrite(UsbInterface iface,byte[] dataPacket, int uartNum) throws UsbException {


        iface.claim();
        try {
            UsbEndpoint endpoint = iface.getUsbEndpoint((byte) 0x01);
            UsbPipe pipe = endpoint.getUsbPipe();
            //System.out.println(pipe);
            pipe.open();
            try {
                byte bufTx[] = new byte[63];

                bufTx = creatPackForSend(dataPacket,uartNum);

                /*
                int lenght = 0;

                if(dataPacket.length<bufTx.length)lenght = dataPacket.length;
                else lenght = bufTx.length-1;

                if(uartNum==3)
                {
                    for (int j = 0; j < lenght; j++)
                        bufTx[j + 2] = dataPacket[j];

                    bufTx[1]=(byte)lenght;
                }
                else
                    for(int j=0; j<lenght;j++) bufTx[j+1]=dataPacket[j];

                uartNum = uartNum & 0x00000003;
                bufTx[0]=(byte)uartNum;

                bufTx[bufTx.length-1] = (byte)Crc8(bufTx,bufTx.length-1);

                //bufTx[1]=100;
                */
                //int sent = pipe.syncSubmit(new byte[]{0x01, 0x01});
                int sent = pipe.syncSubmit(bufTx);
                System.out.println(sent + " bytes sent");
            } finally {
                pipe.close();
            }
        } finally {
            iface.release();
        }

    }

    //===============================================================================================
    public static void usbRead(UsbInterface iface) throws UsbException {

        int i =0;

        while(true) {

            iface.claim();

            UsbEndpoint endpoint = iface.getUsbEndpoint((byte) 0x81);
            UsbPipe pipe = endpoint.getUsbPipe();
            //System.out.println(pipe);
            try
            {
                pipe.open();
                try {
                    byte[] data = new byte[63];
                    int received = pipe.syncSubmit(data);

                    byte crc8 = (byte)Crc8(data,received-1);

                    if(data[received-1]==crc8) {
                        System.out.println(pipe);
                        System.out.print(received + " bytes received, ");
                        System.out.println("CRC8 = " + String.format("0x%x", crc8));
                        System.out.println(prnBytesArrayToString(data, received,false) + "\r\n");
                    }
                    else{
                        System.out.print(received + " ");
                        i++;
                        if(i==50) {
                            i = 0;

                            System.out.print("\r");
                        }
                    }
                } finally {
                    pipe.close();
                }
            }
            finally
            {
                iface.release();
            }

        }
    }
    //===============================================================================================
    public static byte[] usbRead(UsbInterface iface, byte endPoint, boolean readOne) throws UsbException {

        int i =0;

        while(true) {

            iface.claim();

            UsbEndpoint endpoint = iface.getUsbEndpoint(endPoint);
            UsbPipe pipe = endpoint.getUsbPipe();
            //System.out.println(pipe);
            try
            {
                pipe.open();
                try {
                    byte[] data = new byte[63];

                    int received = pipe.syncSubmit(data);

                    //byte crc8 = (byte)Crc8(data,received-1);

                    if(Crc8Check(data,received)) {
                        //System.out.println(pipe);
                        System.out.print("\n" + received + " bytes received, \n");
                        //System.out.println("CRC8 = " + String.format("0x%x", crc8));
                        System.out.println(prnBytesArrayToString(data, received, false) + "\r\n");
                        if(readOne)return data;
                    }
                    else
                        {
                        System.out.print(received + " ");
                        i++;
                        if(i==20) { i = 0;System.out.print("\r"); }
                    }
                } finally {
                    pipe.close();
                }
            }
            finally
            {
                iface.release();
            }
        }
    }

//===============================================================================================
    public static String prnBytesArrayToString(byte [] pack, int length, boolean hexOutputFormat) {

        if(pack==null)return "null";

        String res = "";// = "<<";
        String resHex = "HEX: ";
        byte[] buf = new byte[1];

        try {

            for (int i = 0; i < length; i++)
            {
                resHex = resHex + String.format("%02x ", pack[i]);

                if (pack[i] >= 0)
                {
                    if (pack[i] > (byte)0x20)
                    {
                        res = res + String.format("%c", pack[i]);
                    }
                    else
                    {


                        if (pack[i] == 0)
                        {
                            res = res + ".";
                        }
                        else
                        {
                            if(pack[i]==0x20)
                                res = res + " ";
                            else
                                res = res + String.format(" {%02x}", pack[i]);
                        }
                    }
                }
                else
                {
                    buf[0] = pack[i];
                    res = res + new String(buf, "Cp866");
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
            return "";
        }

        //if((res.charAt(4)=='S')&&(res.charAt(5)!='.'))
        //    res=fiscalDocCmdPackVisualMod(res);

        if(hexOutputFormat)return resHex;
        return res;
    }

//===============================================================================================
    static int Crc8(byte [] pcBlock, int len)
    {
        int crc = 0xFF;

        int pos = 0;

        while (len--!=0)
            crc = Crc8Table[crc ^ pcBlock[pos++]];

        return crc;
    }

    //===============================================================================================
    static int Crc8Hid(byte [] pcBlock, int len)
    {
        int crc = 0xFF;

        int pos = 1;

        while (len--!=0)
            crc = Crc8Table[crc ^ pcBlock[pos++]];

        return crc;
    }

    //===============================================================================================
    static boolean Crc8Check(byte [] pcBlock, int len)
    {
        byte crc8 = (byte)Crc8(pcBlock,pcBlock.length-1);

        if(pcBlock[pcBlock.length-1]==crc8)return true;

        return false;
    }

}